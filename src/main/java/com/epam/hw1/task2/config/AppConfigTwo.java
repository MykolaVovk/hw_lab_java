package com.epam.hw1.task2.config;

import com.epam.hw1.task2.bean.BeanB;
import com.epam.hw1.task2.bean.BeanC;
import com.epam.hw1.task2.bean.BeanD;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
public class AppConfigTwo {

    @Bean(value = "beanC", initMethod = "init", destroyMethod = "destroy")
    @DependsOn({"beanD", "beanB"})
    public BeanC getBeanC() {
        return new BeanC();
    }

    @Bean(value = "beanB", initMethod = "init", destroyMethod = "destroy")
    public BeanB getBeanB() {
        return  new BeanB();
    }

    @Bean(value = "beanD", initMethod = "init", destroyMethod = "destroy")
    public BeanD getBeanD() {
        return new BeanD();
    }
}
