package com.epam.hw1.task2.config;

import com.epam.hw1.task2.MyBeanFactoryPostProcessor;
import com.epam.hw1.task2.MyBeanPostProcessor;
import com.epam.hw1.task2.bean.BeanF;
import org.springframework.context.annotation.*;

@Configuration
@Import(AppConfigTwo.class)
public class AppConfigOne {

    @Bean
    public MyBeanFactoryPostProcessor getMyFactoryPostProcessor() {
        return new MyBeanFactoryPostProcessor();
    }

    @Bean
    public MyBeanPostProcessor getMyPostProcessor() {
        return new MyBeanPostProcessor();
    }

    @Bean("beanF")
    @Lazy
    public BeanF getBeanF() {
        return new BeanF();
    }

}
