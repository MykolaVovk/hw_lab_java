package com.epam.hw1.task2;

import com.epam.hw1.task2.config.AppConfigOne;
import com.epam.hw1.task2.config.AppConfigTwo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext ctx1 = new AnnotationConfigApplicationContext(AppConfigOne.class);
        for (String bean : ctx1.getBeanDefinitionNames()) {
            System.out.println(bean);
        }

        System.out.println("===========");

        ApplicationContext ctx2 = new AnnotationConfigApplicationContext(AppConfigTwo.class);
    }
}
