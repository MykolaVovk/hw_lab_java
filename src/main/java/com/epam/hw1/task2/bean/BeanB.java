package com.epam.hw1.task2.bean;

import com.epam.hw1.task2.MyValidator;
import org.springframework.beans.factory.annotation.Value;

public class BeanB implements MyValidator {
    @Value("${beanB.value}")
    int value;
    @Value("${beanB.name}")
    String name;

    public BeanB() {
        System.out.println("beanB initialized");
    }

    public void init() {
        System.out.println("inside beanB init method");
    }

    public void destroy() {
        System.out.println("inside beanB destroy method");
    }

    public void newInit() {
        System.out.println("inside replaced beanB init method");
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "value=" + value +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public void validate() {
        if (name == null ||
                name.trim().isEmpty() ||
                value < 0) {
            throw new IllegalArgumentException("Instance of BeanB class has not valid fields:" + System.lineSeparator() +
                    "name(not null) = " + name + System.lineSeparator() +
                    "value(>0) = " + value );
        }
    }
}
