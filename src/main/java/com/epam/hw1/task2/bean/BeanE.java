package com.epam.hw1.task2.bean;

import com.epam.hw1.task2.MyValidator;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements MyValidator {
    int value;
    String name;

    public BeanE() {
        System.out.println("beanE initialized");
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "value=" + value +
                ", name='" + name + '\'' +
                '}';
    }

    @PostConstruct
    public void postConstructMethod() {
        System.out.println("inside BeanE postConstruct method");
    }

    @PreDestroy
    public void preDestroyMethod() {
        System.out.println("inside BeanE preDestroy method");
    }

    @Override
    public void validate() {
        if (name == null ||
                name.trim().isEmpty() ||
                value < 0) {
            throw new IllegalArgumentException("Instance of BeanE class has not valid fields:" + System.lineSeparator() +
                    "name(not null) = " + name + System.lineSeparator() +
                    "value(>0) = " + value );
        }
    }
}
