package com.epam.hw1.task2.bean;

import com.epam.hw1.task2.MyValidator;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements InitializingBean, DisposableBean, MyValidator {
    int value;
    String name;

    public BeanA() {
        System.out.println("beanA initialized");
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "value=" + value +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("inside BeanA destroy method");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("inside BeanA afterPropertiesSet method");
    }

    @Override
    public void validate() {
        if (name == null ||
                name.trim().isEmpty() ||
                value < 0) {
            throw new IllegalArgumentException("Instance of BeanA class has not valid fields:" + System.lineSeparator() +
                    "name(not null) = " + name + System.lineSeparator() +
                    "value(>0) = " + value);
        }
    }
}
