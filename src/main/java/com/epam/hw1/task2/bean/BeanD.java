package com.epam.hw1.task2.bean;

import com.epam.hw1.task2.MyValidator;
import org.springframework.beans.factory.annotation.Value;

public class BeanD implements MyValidator {
    @Value("${beanD.value}")
    int value;
    @Value("${beanD.name}")
    String name;

    public BeanD() {
        System.out.println("beanD initialized");
    }

    public void init() {
        System.out.println("inside beanD init method");
    }

    public void destroy() {
        System.out.println("inside beanD destroy method");
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "value=" + value +
                ", name='" + name + '\'' +
                '}';
    }
    @Override
    public void validate() {
        if (name == null ||
                name.trim().isEmpty() ||
                value < 0) {
            throw new IllegalArgumentException("Instance of BeanD class has not valid fields:" + System.lineSeparator() +
                    "name(not null) = " + name + System.lineSeparator() +
                    "value(>0) = " + value );
        }
    }
}
