package com.epam.hw1.task2.bean;

import com.epam.hw1.task2.MyValidator;

public class BeanF implements MyValidator {
    int value;
    String name;

    public BeanF() {
        System.out.println("beanF initialized");
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "value=" + value +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public void validate() {
        if (name == null ||
                name.trim().isEmpty() ||
                value < 0) {
            throw new IllegalArgumentException("Instance of BeanF class has not valid fields:" + System.lineSeparator() +
                    "name(not null) = " + name + System.lineSeparator() +
                    "value(>0) = " + value );
        }
    }
}
