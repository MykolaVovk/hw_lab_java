package com.epam.hw1.task2.bean;

import com.epam.hw1.task2.MyValidator;
import org.springframework.beans.factory.annotation.Value;

public class BeanC implements MyValidator {
    @Value("${beanC.value}")
    int value;
    @Value("${beanC.name}")
    String name;

    public BeanC() {
        System.out.println("beanC initialized");
    }

    public void init() {
        System.out.println("inside beanC init method");
    }

    public void destroy() {
        System.out.println("inside beanC destroy method");
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "value=" + value +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public void validate() {
        if (name == null ||
                name.trim().isEmpty() ||
                value < 0) {
            throw new IllegalArgumentException("Instance of BeanC class has not valid fields:" + System.lineSeparator() +
                    "name(not null) = " + name + System.lineSeparator() +
                    "value(>0) = " + value );
        }
    }
}
