package com.epam.hw1.task1;

import com.epam.hw1.task1.config.AppConfigOne;
import com.epam.hw1.task1.config.AppConfigTwo;
import com.epam.hw1.task1.other.Config;
import com.epam.hw1.task1.zoo.Zoo;
import com.epam.hw1.task1.zoo.ZooConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext ctx1 = new AnnotationConfigApplicationContext(AppConfigOne.class);
        for (String bean : ctx1.getBeanDefinitionNames()) {
            System.out.println(bean);
        }
        System.out.println("===================================");

        ApplicationContext ctx2 = new AnnotationConfigApplicationContext(AppConfigTwo.class);
        for (String bean : ctx2.getBeanDefinitionNames()) {
            System.out.println(bean);
        }
        System.out.println("===================================");

        ApplicationContext ctx3 = new AnnotationConfigApplicationContext(Config.class);
        for (String bean : ctx3.getBeanDefinitionNames()) {
            System.out.println(bean);
        }
        System.out.println("===================================");

        ApplicationContext ctx4 = new AnnotationConfigApplicationContext(ZooConfig.class);
        for (String bean : ctx4.getBeanDefinitionNames()) {
            System.out.println(bean);
        }
        System.out.println("===================================");
        ctx4.getBean(Zoo.class).printSoundsOfZoo();
    }
}
