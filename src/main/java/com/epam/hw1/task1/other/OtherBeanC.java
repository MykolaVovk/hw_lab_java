package com.epam.hw1.task1.other;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("oBeanC")
public class OtherBeanC {
}
