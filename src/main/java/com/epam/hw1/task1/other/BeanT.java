package com.epam.hw1.task1.other;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanT {

    private OtherBeanB beanBT;

    @Autowired
    public void setBeanBT(OtherBeanB beanBT) {
        this.beanBT = beanBT;
    }
}
