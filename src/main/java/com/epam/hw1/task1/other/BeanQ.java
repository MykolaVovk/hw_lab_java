package com.epam.hw1.task1.other;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeanQ {
    private OtherBeanA beanAQ;

    @Autowired
    public BeanQ(@Qualifier("oBeanA") OtherBeanA beanAQ) {
        this.beanAQ = beanAQ;
    }
}
