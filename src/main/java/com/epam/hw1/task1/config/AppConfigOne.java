package com.epam.hw1.task1.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.epam.hw1.task1.beans1")
public class AppConfigOne {
}
