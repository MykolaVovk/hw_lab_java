package com.epam.hw1.task1.config;

import com.epam.hw1.task1.beans3.BeanE;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "com.epam.hw1.task1.beans2")
@ComponentScan(
        basePackages = "com.epam.hw1.task1.beans3",
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = BeanE.class)
        })
public class AppConfigTwo {


}
