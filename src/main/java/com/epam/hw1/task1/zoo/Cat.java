package com.epam.hw1.task1.zoo;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component("kitty")
@Order(3)
public class Cat implements Animal {
    @Override
    public String makeSound() {
        return "meow";
    }
}
