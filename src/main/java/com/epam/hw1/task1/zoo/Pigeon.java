package com.epam.hw1.task1.zoo;

import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
@Primary
public class Pigeon implements Animal {
    @Override
    public String makeSound() {
        return "coo";
    }
}
