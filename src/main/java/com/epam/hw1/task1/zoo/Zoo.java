package com.epam.hw1.task1.zoo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Zoo {

    @Autowired
    private List<Animal> animals;

    public void printSoundsOfZoo() {
        for (Animal animal : animals) {
            System.out.println(animal.makeSound());
        }
    }
}
