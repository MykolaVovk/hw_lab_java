package com.epam.hw1.task1.zoo;

public interface Animal {

    public String makeSound();
}
