package com.epam.hw1.task1.zoo;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component("puppy")
@Order(2)
public class Dog implements Animal {
    @Override
    public String makeSound() {
        return "woof";
    }
}
