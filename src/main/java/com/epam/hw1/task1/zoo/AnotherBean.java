package com.epam.hw1.task1.zoo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class AnotherBean {

    private Animal firstAnimal;
    private Animal secondAnimal;
    private Animal thirdAnimal;

    @Autowired
    public AnotherBean(Animal firstAnimal, @Qualifier("kitty") Animal secondAnimal, @Qualifier("puppy") Animal thirdAnimal) {
        this.firstAnimal = firstAnimal;
        this.secondAnimal = secondAnimal;
        this.thirdAnimal = thirdAnimal;
    }
}
